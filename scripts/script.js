let pushed = document.addEventListener('keydown', function (event) {
    let keys = document.querySelectorAll('.btn');
    keys.forEach(key => {
        if (key.classList.contains('btncolor')) {
            key.classList.remove('btncolor')
        }
        if (event.code[3] === key.getAttribute('data-keyName') || event.code === key.getAttribute('data-keyName')) {
            key.classList.add('btncolor');
        }
    });
})
